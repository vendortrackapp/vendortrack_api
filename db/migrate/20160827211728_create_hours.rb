class CreateHours < ActiveRecord::Migration[5.0]
  def change
    create_table :hours do |t|
      t.integer :day
      t.datetime :open
      t.datetime :close
      t.string :name
      t.decimal :lon
      t.decimal :lat
      t.references :truck, foreign_key: true

      t.timestamps
    end
  end
end
