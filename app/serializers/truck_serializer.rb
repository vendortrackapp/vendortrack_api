class TruckSerializer < ActiveModel::Serializer
  attributes :name, :twitter, :facebook, :website
  has_many :hours
end
