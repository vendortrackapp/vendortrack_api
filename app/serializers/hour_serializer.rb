class HourSerializer < ActiveModel::Serializer
  attributes :day, :open, :close, :name
end
