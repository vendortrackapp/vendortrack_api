class Hour < ApplicationRecord
  belongs_to :truck

  def day
    case self[:day]
    when 0
    	"Sunday"
    when 1
    	"Monday"
    when 2
    	"Tuesday"
    when 3
    	"Wednesday"
    when 4
    	"Thursday"
    when 5
    	"Friday"
    when 6
    	"Saturday"
    end
  end
end
