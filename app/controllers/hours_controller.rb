class HoursController < ApplicationController
  before_action :set_hour, only: [:update, :destroy]

  # GET /truck/{id}/hours
  def index
    @hours = Hour.all

    render json: @hours
  end

  # POST /truck/{id}//hours
  def create
    @hour = Hour.new(hour_params)

    if @hour.save
      render json: @hour, status: :created, location: @hour
    else
      render json: @hour.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /truck/{id}//hours/1
  def update
    if @hour.update(hour_params)
      render json: @hour
    else
      render json: @hour.errors, status: :unprocessable_entity
    end
  end

  # DELETE /truck/{id}/hours/1
  def destroy
    @hour.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hour
      @hour = Hour.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def hour_params
      params[:hour][:truck_id] = params[:truck_id]
      params.require(:hour).permit(:day, :open, :close, :name, :lon, :lat, :truck_id)
    end
end
